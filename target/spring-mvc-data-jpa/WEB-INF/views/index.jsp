<%--
  Created by IntelliJ IDEA.
  User: andy
  Date: 2020-10-09
  Time: 22:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello Spring World</title>
</head>

<body>
<div align="center">
    <h2>${message}</h2>
    <a href="${pageContext.request.contextPath}/home">Go to Home Page for welcome message</a>
    &nbsp;
    <a href="${pageContext.request.contextPath}/pokemanage">Go to pokemon Manager Page</a>
</div>
</body>
</html>
