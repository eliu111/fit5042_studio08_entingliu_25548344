
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$(pageTitle)</title>
</head>
<body style="color:"#b0e0e6";>
    <div align="center" >
        <h2>${message}</h2>
        <p><a href="${pageContext.request.contextPath}/">Go back to Home page</a></p>
    </div>
</body>
</html>
