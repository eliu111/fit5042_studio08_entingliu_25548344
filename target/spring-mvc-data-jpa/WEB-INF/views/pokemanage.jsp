<%--
  Created by IntelliJ IDEA.
  User: andy
  Date: 2020-10-10
  Time: 00:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta http-equlv="Content-Type" content="text/html; charset=UTF-8">
    <title>Pokemon Manager</title>
</head>
<body>

<div align="center">
    <h2>Pokemon Manager</h2>
    <form method="get" action ="search">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search"/>
    </form>
    <h3><a href="${pageContext.request.contextPath}/new">New Pokemon</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            <th>ID</th>
            <th>Pokemon Name</th>
            <th>Pokemon Type</th>
            <th>Poemon Speed</th>
            <th>Action</th>
        </tr>
        <c:forEach items="${listPokemon}" var="pokemon">
            <tr>
                <td>${pokemon.id}</td>
                <td>${pokemon.pokename}</td>
                <td>${pokemon.poketype}</td>
                <td>${pokemon.pokespeed}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/edit?id=${pokemon.id}">Edit</a> &nbsp;&nbsp;&nbsp;
                    <a href="${pageContext.request.contextPath}/delete?id=${pokemon.id}">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <p><a href="${pageContext.request.contextPath}/">Go back to Home page</a></p>
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pokémon_logo.svg/1920px-International_Pokémon_logo.svg.png"
            alt = "Pokemon"
            width = "269"
            height = "99">
</div>
</body>
</html>
